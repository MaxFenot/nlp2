# NLP_projet

Projet realise par Maxime Fenot, Sylvain Blanchard Gaillard et Gillian Deville.

C'est un projet sur l'analyse d'une question en entree qui abouti a une reponse trouvee dans un document.

Nous avons implemente cette solution suivant ces etapes:
-  un modele de question-reponse
-  un modele calculant les les index de recherche pour les corpus.
-  un modele ANN
-  Jonction des parties

Dans les parties de notre solution, nous avons benchmark plusieurs modeles pour avoir les meilleures performances.

Dans la majorite des cas nos reponses semblent en coherence avec les questions posees.

## Installation

Notre projet a ete realise sur plusieurs notebook.
Vous trouverez toutes les dependances requises dans le fichier requirements.txt
Cependant les commandes UNIX  pip install... sont deja implementees dans le notebook.